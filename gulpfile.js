var gulp = require('gulp'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename');

gulp.task('default', function() {
  return gulp.src('wp-content/themes/nataly2015/css/style-common.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename('style-common.min.css'))
    .pipe(gulp.dest('wp-content/themes/nataly2015/css/'));
});
<html>
<body>
	<head>
		<?php
		$real_title = ($_GET['real_title']) ? $_GET['real_title'] : get_the_title();
		
		?>
		
		<title><?php echo $real_title; ?></title>
		<?php $att_image = wp_get_attachment_image_src( $post->id, "thumbnail");

		?>
		<meta property="og:locale" content="ru_RU" />
		<meta property="og:type" content="image" />
		<meta property="og:title" content="<?php echo $real_title; ?>" />
		<meta property="og:url" content="<?php the_permalink(); ?>" />
		<meta property="og:image" content="<?php echo $att_image[0];  ?>" />
	</head>

<script>
	
	function getQueryVariable(variable) {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
	}
	var back_url = getQueryVariable("back_url");
	var real_title = getQueryVariable("real_title");
	
	if (back_url) {
		window.location.href = back_url;
	}
	
</script>

<?php 


if ( have_posts() ) : while ( have_posts() ) : the_post();
$photographer = get_post_meta($post->ID, 'be_photographer_name', true);
$photographerurl = get_post_meta($post->ID, 'be_photographer_url', true);

?>

	<section id="single-project" class="">
		<h1><?php echo $real_title ?></h1>

		<div class="photometa"><span class="photographername"><?php echo $photographer; ?></span> <a href="<?php echo $photographerurl ?>" target="_blank" class="photographerurl"><?php echo $photographerurl ?></a></div>

		<div class="entry-attachment">
			<?php if ( wp_attachment_is_image( $post->id ) ) : $att_image = wp_get_attachment_image_src( $post->id, "middle"); ?>
				<p class="attachment"><a href="<?php echo wp_get_attachment_url($post->id); ?>" title="<?php the_title(); ?>" rel="attachment"><img src="<?php echo $att_image[0];?>" width="<?php echo $att_image[1];?>" height="<?php echo $att_image[2];?>"  class="attachment-medium" alt="<?php $post->post_excerpt; ?>" /></a>
				</p>
			<?php else : ?>
				<a href="<?php echo wp_get_attachment_url($post->ID) ?>" title="<?php echo wp_specialchars( get_the_title($post->ID), 1 ) ?>" rel="attachment"><?php echo basename($post->guid) ?></a>
			<?php endif; ?>
		</div>

		<?php endwhile; ?>

		<?php endif; ?>

	</section>

</body></html>
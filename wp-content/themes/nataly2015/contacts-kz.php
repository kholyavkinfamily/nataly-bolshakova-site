<?php
/**
 * Template name: Contacts
 */
get_header();
$contactfields = get_post_meta( $post->ID, 'contactsfields', true );
$ph1link = str_replace(array(' ', '-', '(', ')'), '', get_phone()['phone_1']);
?>
			<section id="contacts">
				

					<div class="contact-info">
						<h4><?php echo $contactfields[0]['subtitle1']; ?></h4>
						<h3><?php echo $contactfields[0]['subtitle2']; ?></h3>
						<hr class="centered" />
						
						<div class="phone"><a href="tel:<?php echo $ph1link; ?>"><?php echo get_phone()['phone_1']; ?></a></div>
						
						<div class="skype">Skype: <a href="skype:<?php echo $contactfields[0]['skype']; ?>"><?php echo $contactfields[0]['skype']; ?></a></div>
						
						<div class="email"><a href="mailto:<?php echo $contactfields[0]['email']; ?>"><?php echo $contactfields[0]['email']; ?></a></div>
					</div>					

				
		
				
			</section>		

<?php get_footer(); ?>

	


<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'nataly_new');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'r00t');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

define ('WP_MEMORY_LIMIT','128M');

define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME']);
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);
define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6{L!^S}U~-Sm^HM)|yym&m*5S)<^1t.Gf{@P7t91m+83k4t|OH,{OW`#$!wZNtiW');
define('SECURE_AUTH_KEY',  '^n1s eYeFY&]6F-MqZJGPLpN8}Ahsp-dHnvAG<bDb50,3|>-_h9mljv+4<n^e<Ci');
define('LOGGED_IN_KEY',    '0v>z?^|kDBK~cHtA|ynw!t|7*W2C6>g4jBp,!4`_4Gqb[4er)LIAyA,-IgT)PC+Y');
define('NONCE_KEY',        'dDdu]|-:!bbS>+P;}D/Q-HgStYU]5^o{Q3IKC&nv!Z@;_tzi1ffq>k^l[>VoR|-@');
define('AUTH_SALT',        '[*-H]@r5xkY&M+j3Q5xL>!3sm-K6SgQ]A6-wn5e|=7C<:RR)P1JBB99`OPkld#Fz');
define('SECURE_AUTH_SALT', 'uT~m--|!<l_x(A8Ffrq,1+(D+rDN=*m+Azf{,fZ1c_fyZ7W|4C|^Z<>C1FmX8%f2');
define('LOGGED_IN_SALT',   'Erb4t1Td1-s(pic28S[i`%.@WhB1pdW4xLFE~wH+-GFEeH!yf(_s&S!]]rRj}F1<');
define('NONCE_SALT',       ';:R-K;={MF}^ (VMR<=8LZ+d]tvs7pF*}?PS);t8aMF6<M9h_`sGi=J|Yr767Vw=');

/**#@-*/

define('FS_METHOD', 'ftpext');
define('FTP_USER', 'master-nataly');
define('FTP_PASS', 'f_?-j0A#UbY');
define('FTP_HOST', 'nataly-bolshakova.com');
define('FTP_SSL', false);


/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';


define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
